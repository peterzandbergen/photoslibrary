package photosapi

// Follows the implementation of
// https://github.com/googleapis/google-api-go-client/blob/master/adsense/v1.3/adsense-gen.go#L91

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"google.golang.org/api/gensupport"
	googleapi "google.golang.org/api/googleapi"
)

// MediaItemsService exposes the operations on the MediaItems collection.
type MediaItemsService struct {
	s *Service
}

// MediaItemsListCall implements the MediaItems GET method.
type MediaItemsListCall struct {
	s            *Service
	urlParams_   gensupport.URLParams
	ifNoneMatch_ string
	ctx_         context.Context
	header_      http.Header
}

func NewMediaItemsService(s *Service) *MediaItemsService {
	return &MediaItemsService{
		s: s,
	}
}

func (r *MediaItemsService) List() *MediaItemsListCall {
	return &MediaItemsListCall{
		s:          r.s,
		urlParams_: make(gensupport.URLParams),
	}
}

func (c *MediaItemsListCall) PageSize(pageSize int) *MediaItemsListCall {
	c.urlParams_.Set("pageSize", fmt.Sprint(pageSize))
	return c
}

func (c *MediaItemsListCall) PageToken(pageToken string) *MediaItemsListCall {
	c.urlParams_.Set("pageToken", pageToken)
	return c
}

func (c *MediaItemsListCall) Fields(s ...googleapi.Field) *MediaItemsListCall {
	c.urlParams_.Set("fields", googleapi.CombineFields(s))
	return c
}

func (c *MediaItemsListCall) IfNoneMatch(entityTag string) *MediaItemsListCall {
	c.ifNoneMatch_ = entityTag
	return c
}

func (c *MediaItemsListCall) Context(ctx context.Context) *MediaItemsListCall {
	c.ctx_ = ctx
	return c
}

func (c *MediaItemsListCall) Header() http.Header {
	if c.header_ == nil {
		c.header_ = make(http.Header)
	}
	return c.header_
}

func (c *MediaItemsListCall) doRequest(alt string) (*http.Response, error) {
	reqHeaders := make(http.Header)
	for k, v := range c.header_ {
		reqHeaders[k] = v
	}
	reqHeaders.Set("User-Agent", c.s.userAgent())
	if c.ifNoneMatch_ != "" {
		reqHeaders.Set("If-None-Match", c.ifNoneMatch_)
	}
	var body io.Reader
	c.urlParams_.Set("alt", alt)
	c.urlParams_.Set("prettyPrint", "false")
	urls := googleapi.ResolveRelative(c.s.BasePath, mediaItemsResource)
	urls += "?" + c.urlParams_.Encode()
	req, err := http.NewRequest("GET", urls, body)
	if err != nil {
		return nil, err
	}
	req.Header = reqHeaders
	return gensupport.SendRequest(c.ctx_, c.s.client, req)
}

func (c *MediaItemsListCall) Do(opts ...googleapi.CallOption) (*MediaItems, error) {
	gensupport.SetOptions(c.urlParams_, opts...)
	res, err := c.doRequest("json")
	if res != nil && res.StatusCode == http.StatusNotModified {
		if res.Body != nil {
			res.Body.Close()
		}
		return nil, &googleapi.Error{
			Code:   res.StatusCode,
			Header: res.Header,
		}
	}
	if err != nil {
		return nil, err
	}
	defer googleapi.CloseBody(res)
	if err := googleapi.CheckResponse(res); err != nil {
		return nil, err
	}
	ret := &MediaItems{
		ServerResponse: googleapi.ServerResponse{
			Header:         res.Header,
			HTTPStatusCode: res.StatusCode,
		},
	}
	target := &ret
	if err := gensupport.DecodeResponse(target, res); err != nil {
		return nil, err
	}
	return ret, nil
}

type MediaItemsGetCall struct {
	s            *Service
	id           string
	urlParams_   gensupport.URLParams
	ifNoneMatch_ string
	ctx_         context.Context
	header_      http.Header
}

func (r *MediaItemsService) Get(id string) *MediaItemsGetCall {
	return &MediaItemsGetCall{
		s:          r.s,
		urlParams_: make(gensupport.URLParams),
		id:         id,
	}
}

func (c *MediaItemsGetCall) Fields(s ...googleapi.Field) *MediaItemsGetCall {
	c.urlParams_.Set("fields", googleapi.CombineFields(s))
	return c
}

func (c *MediaItemsGetCall) Context(ctx context.Context) *MediaItemsGetCall {
	c.ctx_ = ctx
	return c
}

func (c *MediaItemsGetCall) doRequest(alt string) (*http.Response, error) {
	reqHeaders := make(http.Header)
	for k, v := range c.header_ {
		reqHeaders[k] = v
	}
	reqHeaders.Set("User-Agent", c.s.userAgent())
	if c.ifNoneMatch_ != "" {
		reqHeaders.Set("If-None-Match", c.ifNoneMatch_)
	}
	var body io.Reader
	c.urlParams_.Set("alt", alt)
	c.urlParams_.Set("prettyPrint", "false")
	// TODO: find the parameter name.
	urls := googleapi.ResolveRelative(c.s.BasePath, mediaItemsResource+"/{mediaItemId}")
	urls += "?" + c.urlParams_.Encode()
	req, err := http.NewRequest("GET", urls, body)
	if err != nil {
		return nil, err
	}
	req.Header = reqHeaders
	googleapi.Expand(req.URL, map[string]string{
		"mediaItemId": c.id,
	})
	return gensupport.SendRequest(c.ctx_, c.s.client, req)
}

func (c *MediaItemsGetCall) Do(opts ...googleapi.CallOption) (*MediaItem, error) {
	gensupport.SetOptions(c.urlParams_, opts...)
	res, err := c.doRequest("json")
	if res != nil && res.StatusCode == http.StatusNotModified {
		if res.Body != nil {
			res.Body.Close()
		}
		return nil, &googleapi.Error{
			Code:   res.StatusCode,
			Header: res.Header,
		}
	}
	if err != nil {
		return nil, err
	}
	defer googleapi.CloseBody(res)
	if err := googleapi.CheckResponse(res); err != nil {
		return nil, err
	}
	ret := &MediaItem{
		ServerResponse: googleapi.ServerResponse{
			Header:         res.Header,
			HTTPStatusCode: res.StatusCode,
		},
	}
	target := &ret
	if err := gensupport.DecodeResponse(target, res); err != nil {
		return nil, err
	}
	return ret, nil
}

type MediaItem struct {
	Id              string           `json:"id"`
	Description     string           `json:"description,omitempty"`
	ProductUrl      string           `json:"productUrl,omitempty"`
	BaseUrl         string           `json:"baseUrl,omitempty"`
	MimeType        string           `json:"mimeType,omitempty"`
	MediaMetadata   *MediaMetadata   `json:"mediaMetadata,omitempty"`
	ContributorInfo *ContributorInfo `json:"contributorInfo,omitempty"`
	FileName        string           `json:"filename,omitempty"`

	googleapi.ServerResponse `json:"-"`
}

type MediaMetadata struct {
	CreationTime string `json:"creationTime,omitempty"`
	Width        string `json:"width,omitempty"`
	Height       string `json:"height,omitempty"`
	Photo        *Photo `json:"photo,omitempty"`
	Video        *Video `json:"video,omitempty"`
}

type ContributorInfo struct {
	ProfilePictureBaseUrl string `json:"profilePictureBaseUrl,omitempty""`
	DisplayName           string `json:"displayName,omitempty""`
}

type Photo struct {
	CameraMake      string  `json:"cameraMake"`
	CameraModel     string  `json:"cameraModel"`
	FocalLength     float64 `json:"focalLength"`
	ApertureFNumber float64 `json:"apertureFNumber"`
	IsoEquivalent   float64 `json:"isoEquivalent"`
	ExposureTime    string  `json:"exposureTime"`
}

type Video struct {
	CameraMake  string  `json:"cameraMake"`
	CameraModel string  `json:"cameraModel"`
	Fps         float64 `json:"fps"`
	Status      string  `json:"status"`
}

type MediaItems struct {
	Items []*MediaItem `json:"mediaItems,omitempty"`

	NextPageToken string `json:"nextPageToken,omitempty"`

	googleapi.ServerResponse `json:"-"`
}
