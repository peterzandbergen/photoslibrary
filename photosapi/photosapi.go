package photosapi

import (
	"context"
	"errors"
	"net/http"

	googleapi "google.golang.org/api/googleapi"
	"google.golang.org/api/option"
	htransport "google.golang.org/api/transport/http"
)

// OAuth2 scopes used by this API.
const (
	// Read the library
	PhotosLibraryReadOnlyScope = "https://www.googleapis.com/auth/photoslibrary.readonly"

	// WriteOnly scope
	PhotosLibraryWriteOnlyScope = "https://www.googleapis.com/auth/photoslibrary.appendonly"

	PhotosLibrarySharingScope = "https://www.googleapis.com/auth/photoslibrary.sharing"

	PhotosLibraryReadOnlyDeveloperScope = "https://www.googleapis.com/auth/photoslibrary.readonly.appcreateddata"
)

const (
	basePath             = "https://photoslibrary.googleapis.com"
	albumsResource       = "v1/albums"
	mediaItemsResource   = "v1/mediaItems"
	sharedAlbumsResource = "v1/sharedAlbums"
	searchMethod         = ":search"
)

type Service struct {
	client    *http.Client
	BasePath  string // API endpoint base URL
	UserAgent string // optional additional User-Agent fragment

	MediaItems   *MediaItemsService
	Albums       *AlbumsService
	SharedAlbums *SharedAlbumsService
}

// NewService creates a new Service.
func NewService(ctx context.Context, opts ...option.ClientOption) (*Service, error) {
	scopesOption := option.WithScopes(
		PhotosLibraryReadOnlyScope,
	)
	// NOTE: prepend, so we don't override user-specified scopes.
	opts = append([]option.ClientOption{scopesOption}, opts...)
	client, endpoint, err := htransport.NewClient(ctx, opts...)
	if err != nil {
		return nil, err
	}
	s, err := New(client)
	if err != nil {
		return nil, err
	}
	if endpoint != "" {
		s.BasePath = endpoint
	}
	return s, nil
}

func (s *Service) userAgent() string {
	if s.UserAgent == "" {
		return googleapi.UserAgent
	}
	return googleapi.UserAgent + " " + s.UserAgent
}

func New(client *http.Client) (*Service, error) {
	if client == nil {
		return nil, errors.New("client is nil")
	}
	s := &Service{client: client, BasePath: basePath}

	s.MediaItems = NewMediaItemsService(s)

	return s, nil
}
