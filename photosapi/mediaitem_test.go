package photosapi

import (
	"bytes"
	"encoding/json"
	"testing"
)

const response = `{
	"mediaItems": [
	  {
		"id": "AIG8HdZysjcSTMfDLJJpthUaJMq_0s1ehhCz7Y59-MIgtG7vKbUGgO5PHUs07rjrl9K-GnZ4eL2MAPVrcGiw9kdjIh4XHy8V5A",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdZysjcSTMfDLJJpthUaJMq_0s1ehhCz7Y59-MIgtG7vKbUGgO5PHUs07rjrl9K-GnZ4eL2MAPVrcGiw9kdjIh4XHy8V5A",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e5hnPsILXWmPLWYeY09v8QsAGfM2ol5dani_uotucUEF2r2nCN1KXqYcrRJ81dbHS6Ypxrdl8oU6wQ658TC6JAXhz1Arr7g4ZOoqxmgvvRic92soMy-NuC_OToI3PtECGOFofYDWgK0slZwdLH_w_0htdxG2tkG-ZCE2X_0kcYzjcmSzyqqIjG1881QyLYCQn1kr8NeLKr5TK0krh66zh5atj-lKzLmzkD9i8TLIlnnAc4-sG8pTWNb66iZsvepY6S4V_zKbcWALA_lYFrrD642IK1BHGsJ5VxGGUuzg09E1vy0TARdkUIX1EM-SlNqcNWP2bQW7Qrf4ws_cNuw6GD4a1-g863KZuPt7-pCaYn1ftBVuZPVDrfeBg9Hj5ieRoWWRhXnywfqlGb02Gy1kremD7Z1QqCYYm_t0HdahjpODN3POYcnfteCqjrmXJ0ZnWiK2TTRgAFG5zEFicZqmYWdIxpWdoO44d82eTBsmeFlRZW4KriAJa3GLth7E7b3Z8uvVcQy5yccHr27vpThE6kHEgWPlJP5vRJL3FUZf3btQGU58zkIVK_fvmAFJthTMGKtR1Bjs_W7qCI4Dfp-DPQ6yu59ap0T5rd1aPM7flTbDvmBlR3kCG_FhxJJ_YVT1xQb91u6cgFlv4HI3UrsIJIGGfbLVEvgNktXjCmcmq68q30cXc4_KIP8s4t2vcI7uadVu6sR2NICLf7KVdGvuwTwrg-kqil5zAQL4H4_bNBmm7U03ESId6FBEPWtIHjrPzpSTnfGI76GYV0nkZwlBFwb350Pko63KvzF-SUnl466VT7UJUewBzjNK9E",
		"mimeType": "image/jpeg",
		"mediaMetadata": {
		  "creationTime": "2019-07-01T15:27:24Z",
		  "width": "4032",
		  "height": "3024",
		  "photo": {
			"cameraMake": "motorola",
			"cameraModel": "Moto G (5) Plus",
			"focalLength": 4.28,
			"apertureFNumber": 1.7,
			"isoEquivalent": 250
		  }
		},
		"filename": "IMG_20190701_172724501.jpg"
	  },
	  {
		"id": "AIG8HdZXhbyVbawxfpBN9HSt5FE2Gn90Glci7G7-jGZ2PQ1OnMweQ7gzKx5qbw9ENaAj-QfdUC-TF_XaoJ7buTuOSnJp8D5tmw",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdZXhbyVbawxfpBN9HSt5FE2Gn90Glci7G7-jGZ2PQ1OnMweQ7gzKx5qbw9ENaAj-QfdUC-TF_XaoJ7buTuOSnJp8D5tmw",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e6ohWMEFrQJuEe7q03jX8TothMuEeve6CKX10XzWoSPcSBqzp-giMtAklQAv5REsWSGlnoKJmUTZjEokbXNSdPziqu4xT9hLwXo9fUfdo9lZYGqfoUBc02dtFD4J6GCtyWhaRqFOASvfHHLSz53ysLYk0xntIQfW-oa8Cbf4nct3BcYGpFB1UDb4qGCyulmh6qzmFYtZJeMDUZiSOs58-YCsJ1DZFehvC2pFLt7FbYkVdnaSlxKjiV2VAFJZKqAt6KaV9_r6ud7OHKzwLYqIV9G_jIfURgcrjqIM_bsj3lKgQAWFmcJX6b2GNc0WI6wEBYtgqt4DFK_ETCeUY-RnaesVrOqMZ_u3QE7qPcmAG3TNQZH8NP4uOZeo2dnHKzY3KHbEmndgSSi42eQSPqw5Hm73fwFesP4mj8N8Ypg1u-UavwL81Dc-lfLLIupk0oXp0cwiivAEM3nKoz8k_QJKpON8DqgHpHJLB9-tB5JsgdAdWCOeIBzxYuVMPt5vqw92gCgv8c8-WYhNRiety3eLiBcSHwv5kwilxVyQkj4D0PiVBUuBnuRxH72GPV6-Lz0tCCh0hQFDJqnKEkiPCZ5e8X2j5wNYdxHPt6UKnTDwOjWPzEUHuZ_iaU4ssdZQfnVD3wRtwqIWu2H6D5bawztjiBIfvAH_LsP_pKOwq8OSynLlQI4z_NuDWfOJ7sIqPvWBnPHRtJACHNmf3Eg6-XZ5_PvZ24dgTfsIDwgS3ruXGXATTO-7t2VSNH9OPOzKiVxhMWn7QEa7BQnUJ6zlgIArJyuR2EBCMXunhXgyq72xE54XEXJHEO2FKbJEU4",
		"mimeType": "image/jpeg",
		"mediaMetadata": {
		  "creationTime": "2019-07-01T15:11:37Z",
		  "width": "1599",
		  "height": "1200",
		  "photo": {}
		},
		"filename": "IMG-20190701-WA0002.jpg"
	  },
	  {
		"id": "AIG8HdazNzmWIJBTrz7KPad3R4ma3cgtd0LaEuILXxIGrk2mW5GcR-j_Kzarkb651gWaXqlE1dyOX3vGSJ28NK_YXLIGJhJhSg",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdazNzmWIJBTrz7KPad3R4ma3cgtd0LaEuILXxIGrk2mW5GcR-j_Kzarkb651gWaXqlE1dyOX3vGSJ28NK_YXLIGJhJhSg",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e4n_AVfLLDrJ5v3JKCKZSSZYdvf_E10V5iIBpQXbioRTVIcGeDpBUmq8iXkyGNWnsk8qWSrgMOSSfXJc5PRusIMV5khXJRlP08IBQzsgRMuWlGE5QWph_EoeOsxq-QM1do9yuRfi8tR-B1WnR-30VjdPx_MB_oirbew0Q0kJRxSbMslzDgzHq93DyUIhsOlMMNGJZD39QRydQqZnSn0EuN_FkLSdvOLP0XVjIKpsmL2D9-KbWh2DtlYvpB46CdJOiyLaA0LbMGutIwLkM7FSNq5pN2_wAnSdYxtftLyVTwaIIyeVowl4ZX_CuSW8UAfQqHsuhsw6s4goQ6150rhLAciS22c0b8utHnwwpxEgeltzecdYtuGGVUnpnkYYW6-LKYTHYk0M8VzYxvcWMPmQ_QcS-xyQmQExr6NiIj5HsLF6alQJ1LGQjTlVnps0ybmh0sU9gHQ-9Nge5KjkBBys8XL0bzplr7uIR7HzFfCAPd1t7E6rfqKXSVeI0XidHqP7PDvuYcSpaJeHXpOcyj0_YFghMmWYX1FD2tToB9LktUAPVOxdmWu7H4iRaSnHoX2tLnuY_wjGo5lG70lGbO9SPRPXhFBWXjRv9Yru5wE5RnWMvo-i5FLax2JFgQ3NlpWO53-DBTRGQ2uw0ALAl6XqUFX6pnn77WRsMSVuKAAxsG9QuYqlbJ6_oICQU8lWO5VLTJrjaJGv-H5cZNfOTn3myA8Yucf53rvg0wlFRgAjFrADexJpIkhHBIF3-BJd-qX_P8sV4cjKhWwL300cx2qAvzTXunoP3g5MMFqFW-am9c-tume6-pw5Y-ZbQI",
		"mimeType": "video/mp4",
		"mediaMetadata": {
		  "creationTime": "2019-07-01T07:30:45Z",
		  "width": "1280",
		  "height": "720",
		  "video": {
			"fps": 29.979918664275431,
			"status": "READY"
		  }
		},
		"filename": "VID-20190701-WA0000.mp4"
	  },
	  {
		"id": "AIG8HdYDt0oDvoq3rXPq-IRQP472XPd6uNFk57Qf3jisGXK25PkpzdNNTVvxFA_CiOfyhFsRpkgGpK452IrJLREkRcShkzITVQ",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdYDt0oDvoq3rXPq-IRQP472XPd6uNFk57Qf3jisGXK25PkpzdNNTVvxFA_CiOfyhFsRpkgGpK452IrJLREkRcShkzITVQ",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e7N_oKpytgK1SSJbmdpiigb33pCUqqS0uclnerf5RMcG0h241ZqulwSam0xDAntOnJFY_dx4DmKJRi2cL6-jDACSbWA-x4d37bRWqLXg1C2YA0EWD3lLpPJCGAUzQtuIoV52PFOFmRSkTeXHe32Vxd3vH6UrUVE1jmEl1kklnsO0Nq2sZQR1d8yHx35uIONTZ7VmKxcyzQWYHiq6r-0TGc78L6NzG87Lv7C9z4Kr2LazmiajsRkrjN0SlirK5_lVr3bs4PtixoC09ywUTFFFdT7X9LEEqygp6mhyKF8bfnHMmIAeTKkgNuKWvHym0HYLuCFAl23p4-fgvqnkSm-muPLxvl7FiRHGi5G78PWV1fzMOqg79OPm06T5G646lPa2DyIMGGYvq75iF9z2ZmIqubAdwwWu7ChN6gVf5pVAaLJRnf042lKehHmfiW9d4x7lnVtZMDIcjqi1hWhBGS2jO9sHyTpcpccLgqpKuCeX2sjWOq0Pw7XIqZ5N02MBwRCZfTr20A-41W1YijqKUhDtwKRpBDU00wiEa_Ice3VSpruZxSkEM5zomL4hm9pxzbJ9K3igSG4LOYAgjoIbFUqHzna298q_ZJ6VKPgeZ4VmOrYxgx__Jak4BHDCx2_l5UjDtDJ8BvpiaV7oUxKsYWAQUqil5M9bEChfqTScAAX_8rHXfOGAI2Apm48PMFoX0zhLX37R8FSBeVnxoqjwbA3Vzl0yqcvpeX0bV2UDIPzgZL0yAiA_QDhLYtrB25W7RTlQEVQd4saYxy9dQM2_28gkpGVPSSYRRaD_jfiYKvigtrHupcXthQ2p_mHYb0",
		"mimeType": "image/jpeg",
		"mediaMetadata": {
		  "creationTime": "2019-06-30T17:00:24Z",
		  "width": "576",
		  "height": "1024",
		  "photo": {}
		},
		"filename": "IMG-20190630-WA0001.jpg"
	  },
	  {
		"id": "AIG8Hdb0BOH5LM4iE-wTa7JeOBs1nih7GENHis_DEMGSF2fH3UbYLeuY5IXBspkq4koK2MOfzLKgA7gg9_qToZ_1wDuv3ebvnA",
		"productUrl": "https://photos.google.com/lr/photo/AIG8Hdb0BOH5LM4iE-wTa7JeOBs1nih7GENHis_DEMGSF2fH3UbYLeuY5IXBspkq4koK2MOfzLKgA7gg9_qToZ_1wDuv3ebvnA",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e5UqCBtzrnhMncMWhFprxSzF0odmPoJqQeyJyegpxvy57nOg8NH95ebnSpZKB3AtSlH7rRfy4qrVkl5nqKE19RnnKC7Dvo8irGJR0eZrTLABbjJXw4YadT7vcNzR6EFsg-lNhLSg7nAB_54SwYPP928HpDXMsGIwDQEijadSAS3SEP-IO5wVMlDHOE927YCGemozI114j25MMdIYWYnDLBYBvmJ8dkdokzQl_bsBEPbstWst39Q91PKkndOtbKB2M47NLOijRVogcS2hPuPOlrGxhbq6utzCj1-U1aqOx9WhcPRn7O-EDDK6LeVyXZ4vCQY7o9s1UAm4_Yi8X5MWCGHKtQYcpGYmoenh3DhjV3TOK-InmRzwYp283d75fyqsjOCr77O9FQjsiXDaySCzKf-3ZXEbO8EJ4RuoB0pzHbDEFEbc5NppoWf4z0g2l3imc7dkRo0Fk__MsJ0gHhC1ZrprZ5flNKowxX9ANJfjRg3yhxrlkIE6TzSP9Yvatkxzz1amSKfHFCWF-TTGxRHHJjRIL8QQV2LUfguu7afNSfTPFh8mwl-QNSdCLziSEo8aivhsbnaVu8wCZbyv_86iZ_Y7hjZWHzZtJeY57YuFilP1NqJn8hZ20P1lrSQCrJqKLYo82kLOY7pLb0CHacBNSg3POT39mCTXU-3EC6YRv88QXFt4_vV7f4b-oZTjFVYN4B41YTxCpBKGM9CqU9SmR31Evs5rH9FeuQgLt4znVWZvtlf_vU50GK1npI1SuEEuVSdPnDdo2EzoM1YwjItuVONJ-rziZwhecgjY_qLxwxEklpQS9h3K09-Riw",
		"mimeType": "image/jpeg",
		"mediaMetadata": {
		  "creationTime": "2019-06-30T14:58:03Z",
		  "width": "3024",
		  "height": "4032",
		  "photo": {
			"cameraMake": "motorola",
			"cameraModel": "Moto G (5) Plus",
			"focalLength": 4.28,
			"apertureFNumber": 1.7,
			"isoEquivalent": 100
		  }
		},
		"filename": "IMG_20190630_165803759.jpg"
	  },
	  {
		"id": "AIG8HdYGOddV3TKQilJsa3UOt12jmQTBrGXvzBK6dDbSPYsctht-i9CYcvrvaObVb7Ckcqqydpv7NzYEY-ssFYr1A4hzyNRedQ",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdYGOddV3TKQilJsa3UOt12jmQTBrGXvzBK6dDbSPYsctht-i9CYcvrvaObVb7Ckcqqydpv7NzYEY-ssFYr1A4hzyNRedQ",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e7WAw1RvkjR72C0E5PchFaeD_9bizzUP7s377IAcLqTIPpFJ5POEenEBmX5jowU17spjN8zsCNepo1j2VrJl7NaHoeIbDsMGpGTBpI24XSBp_dhqoOziIuonkvLBeM08VAFGD_X2LNvNhbWVKApwIzN64M7VhzUKPbly5YHYALyf7nmiSpscJoFczR1EgOfujCVBnSHb05NRU1BgsgM0c47B029qEJ_5Mf-f7XkQ1fdJzQEiwK8aQKZPGryTTLht0rwubPk5y1k8UNo6wf9QqfKga_wifwV83w0eFu6ydbegB7vbPNJGDMni9NqdcWnyBZ883WRE3NT2M_9Nayz3-PrtSD-FGEr_9dT7PaZ8tpUK8kE9rnKHSj2FPaskhQlyPlnAfF0tp9g3tqobiZwdRj_PAxidvpPGerlzNDApewvAEnNPcQKdW9Htnsqc-pLfJOfgT0NbaCA4JxAbYnrlDRVv3zvemV6wT91-1iNajbiA2dBUgbuaAsAuwB0J7G-LqeUmmL2to1ZehxGKYHs1F-relkaQ9RjY-wBp9vZp1ksr1tTcjtZvonPfpFlyKmS6PNjhtw5HA6cbbRZ_uzUyVWqvDmGNVEh5dhWT481L8CGiEnfIx416YIbgS7Zaft3FdLLUXp7U0lWMJNZMdKo2aWWkUQimvOkEN12aHDuRmnFK6cr8lay8tCB05OMkuAI4qSgo59uXp4Az3ik4GG2zYo2MZkiJD1DHuLBEoWfclIMxVTtACLsXQXHNobzlrubmUDZsadKNdhCcLlkeGgL486Rt6kzx_sMke5QQOPCK_RKcN0CevCb0SY-2SA",
		"mimeType": "video/mp4",
		"mediaMetadata": {
		  "creationTime": "2019-06-30T09:40:22Z",
		  "width": "1920",
		  "height": "1080",
		  "video": {
			"fps": 29.829740399183013,
			"status": "READY"
		  }
		},
		"filename": "VID_20190630_113930400.mp4"
	  },
	  {
		"id": "AIG8HdY_-9W8a5VETOB4rnmTFl8Vysa0IDyZIQgl57ZBnk4ZImsTGuzURom3gSPqXgE-Yj4IYgX2sEzdzMKm9FmdXYyFiWDsew",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdY_-9W8a5VETOB4rnmTFl8Vysa0IDyZIQgl57ZBnk4ZImsTGuzURom3gSPqXgE-Yj4IYgX2sEzdzMKm9FmdXYyFiWDsew",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e5RBpvc6nHwTjpabsbofCbsVQL_OIpL8Lohw2pNLZBUCn3fXbPE9ZUGjPkFiDDx30YtbPtG4MTgqNqOywaH9fJLhiUiykbe4uaKOAuL7LEkuWy_OIVBHDirk-Gpq9fhx3xWlW1Rw7K0Bof0vfs7FTkYpM_mXNY7ejUHH2kB8N1QGgzD4r8oHZ3s9lLIin_zHSppKEwrV9yRv7QzIdhouhdbUsvtFiOEqLyS7NSeYrw7eIWjRwPn3-q7rkPyr5Jrz6SH95qY01Dg3PxQry4rU3zgJxt4ozBLryZZEU_ePriGADrz3-B8WHCJBRKc2586CAszmzJF5oNYGsWP_A5XB7Evr74KgoQH9x6lsUcQFUQd4nNIye0BlGPgm8cOlHLhA3MGjrHqQvYg7zvBPdbxvWvFgJp_vpvm5wWUcBenvqmFxs7rfsWQtvXQZB-EH_GW-zOcoVzB_UHTqbAK3kOIz2i6IvgxGeg2UkCn00v1IC61DEdxe-iXv8owQOnbg1sQ7i0rsV0dR1ReJDXeH8DGO2qQYlLcoe6PkZMhWapMTMXiiG3yvZjmLsDwRvlXGTAaz2h5yxay7sr35kVzTxv43hIW9jgBNeNZiywBuP3iwuymgQoo1-AvZgypCFAzuLg7m28EgJnywQclm_tVt4_bjfox8Oybu000RyeOkEl6H8mlfAxxvoIbokcOeN3IAgSsTwReAFdK5Vlsai2nAvzAHMV1_pCdwe1H8MIeqjXONKwqGeU0-L2WFKMfZ2aiVArUs8hrkMWNL3jzsi2-wYMpVbCbM3fN98VnvPsFAcCV0aMrwIBugLz4q-K4y-8",
		"mimeType": "video/mp4",
		"mediaMetadata": {
		  "creationTime": "2019-06-30T09:39:28Z",
		  "width": "1920",
		  "height": "1080",
		  "video": {
			"fps": 29.980841511034413,
			"status": "READY"
		  }
		},
		"filename": "VID_20190630_113919120.mp4"
	  },
	  {
		"id": "AIG8HdZF1vT7U0r_WaaDU_f3iK6ugbzsD8flWsulhfm1iC6DKZcHlPpPvfUOs_w2i0o5AXMwzwZxMpeOJ_IkhVSaqkbxLqOxZw",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdZF1vT7U0r_WaaDU_f3iK6ugbzsD8flWsulhfm1iC6DKZcHlPpPvfUOs_w2i0o5AXMwzwZxMpeOJ_IkhVSaqkbxLqOxZw",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e5cowwuMsfOQDPITnbnda_rkj5zs42KlesYwp12tp14aOTip00hYSrNJN4-IhiODYA3WLTnaeegNJuHNdufoD43JGw0JshE8nZIbHTnyKBfUoJo8HVtOU0dYlWjkzfh3UOaMWZyeYeZX-SQD0iQxMRd_wE1kOSKIN_08_TGh5TpUcc1VZrjhVm_T5NoNh8f54KR2Hf96p7qHshXGeooH92Zhm2PrpCDlojcchqc-6Bvi-dYAhVUBjzNl3imzdbteSn9wyuVmR1ajtDqT4wv9aka0n3JiVTuOFluGKhF-ADxm7BUR8pM5XbAeyo09N625FtUyWyRAkVMeV6Tb53BC97MnVveQDXdvFlmTErXknPABRFQw2B0nuVe_6XTCwW-CnGQ14jpUPsncck9Ojlh4BxYJU_59kqSojOc650kViMdEp5jxJcbUVYh5tz18ajOelO58zXiz6EE-a1yURQwG7kWl10uzwiqMgsEU9yv4WxMJRCN1jZm4S7tuDDzGNijAgBt5UqnL1owkQ0PWCl9oABICxNIaYNz9SnV27O_bvJz4dhWzksvMQwGg9qorfk_K2oIdkf2cPdxXpP2NwTuFvl54VyNzYnlAMZiCMEdxMGjnbQ1CEAiJwlFBahgk14WoWnR0l_G4ZZTxoTeccrMk5NRzIfJBVMJHQlKic1lr2os8t-9WGFkndhqJCl7uFuz1ECykNlSCx-Mn9c9cUTzTSUbn-lwYi9swW6glfWlObSRU8V02T0mzn_0pWpMx6ntkZPVt-ev1xcXInbmpXM71zM5QR-ptGvyTmqbzcYzkHnEM7zytxE0CoIic8A",
		"mimeType": "video/mp4",
		"mediaMetadata": {
		  "creationTime": "2019-06-30T09:32:41Z",
		  "width": "886",
		  "height": "1920",
		  "video": {
			"fps": 38.545682775934729,
			"status": "READY"
		  }
		},
		"filename": "VID-20190630-WA0000.mp4"
	  },
	  {
		"id": "AIG8HdYeMGAab7LJHihTUWptXMUaN4tAGHAjusA0zcC37qUGfavacl7_RS8dERM9L4N6r5FuwD-1Y3RCpmLzKezi1aKsiRtvHA",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdYeMGAab7LJHihTUWptXMUaN4tAGHAjusA0zcC37qUGfavacl7_RS8dERM9L4N6r5FuwD-1Y3RCpmLzKezi1aKsiRtvHA",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e5AU1GtCz11BlWtBXsAjaKp1DmMLojusvevfb7FPsi3tpTShPG8NlNUMupTmia_obwNPfyL026fxkQYXflTtX6eQM4RCh8ShlLIvo5i1Z73m8TkcwBM_CoeoIowSkuFeSO6yDGQazykCcXltMXduyPen21TEiA71U9KPclqh89QBrcLkuGejqCArwcTQV947jyhNQ2-7bI46cnfldOuHlBDb-R8U-R4ffWArsJUgqhgpvhgEpXrEgfZE8hm-opMCKiUj7VUuVQ4iZav4ozGGSE3ngNjiYyukwYbZla1TrDgokBoIsqo2cue4unC9XxxkwVfm7x4sFff5VJ2o_A-gEJdYofOHOTUBL18fvImFIBE2ov1hqOHIOJJ3Fd-1Dxw_MqSIFMjxgzVKLmAm-XQ5eaNB174JnMgrs1QCBfHgTy3EgE5gez116-gomC8tNpwXC284gAA-YKLLCp4JrtCRYEWryeDAPh_q3tVdKq3wkSpcFuTqXgHF6CbOm6rP17n7pJ6Ek_lQJorhraKBe6IMt82z0hlGI5oYx3HhsitWfX2955cwYyDmf5ufkgKfZN2PSNk164ClpR8TRYBmuhBFw01ynL2reIPNqJDg81krgR_nw56RE1JE-2sE3lvp9JQnO03QX1Tk_7DDEzxHujYJEe6Fe5vEQ5yU5hXTFmLxWc5UXLAZxHmgayrgKgR4V90__CFQA-BZMOVRC24e2XoQJIYvXJseQs8JbA--P0_bd-cyz6zgc_3OUj5AR1Bo53PAntg8iS3teAuKeK2lM2rgpGHMdL2AGs4YcVrNvpIbHNnqbZDU_48f8L_KVs",
		"mimeType": "video/mp4",
		"mediaMetadata": {
		  "creationTime": "2019-06-29T20:33:09Z",
		  "width": "848",
		  "height": "480",
		  "video": {
			"fps": 29.88601227560109,
			"status": "READY"
		  }
		},
		"filename": "VID-20190629-WA0026.mp4"
	  },
	  {
		"id": "AIG8HdZRjIZTSXtpPmS3siiswZ_y0vQA8Q2-iB9Y-iSAW2z91bgdw2pBsZeMSUVT4JLMFZtOAuypcod_Jb8AkOXpLFKeo5Dg2BHDkiejh1dVrsp6gNhHioo",
		"productUrl": "https://photos.google.com/lr/photo/AIG8HdZRjIZTSXtpPmS3siiswZ_y0vQA8Q2-iB9Y-iSAW2z91bgdw2pBsZeMSUVT4JLMFZtOAuypcod_Jb8AkOXpLFKeo5Dg2BHDkiejh1dVrsp6gNhHioo",
		"baseUrl": "https://lh3.googleusercontent.com/lr/AGWb-e6SEmxpvEhots9HH6weFUbVC1FapcgWmQ7Q0d31DUaAhUZRXAPERvy1BoYarbqBMRtf72PvFgwrb60AGKAUx0e651qXsugBm1dkmHwyWLEcXfBwqckASc8Cpy4a1DFjHhhTRhq4JYC0BjKhhpqvxRpcr6CcHzfirb-JCpQRFbN5aL05STzO4-s2aBwpvzV7cLfnBlqGfgC--BNVPjq9_Sw4isk6Yyj5bcbtkphBhHqAyIz1SBT16G-200i8CgAdMXKlt0ri4m0hHOVVrOKRAlrqhE6prEyJNNvcqVHMyThkHcAweDNaDaL9DvGG-6tXHjIEVH9Im76J_ilibfiszMbca2_GJUkQoKsk3rdkBFukjrzsA9CEufV2uOaRAFdl6Ryn1P5p2v8pgld-hCgeH4ECLnT0AyTFqFIoAwapBvruRNAmuMLn7J84qLvnPev_Wu6yS9gK7j5dujabeNK1-VfXG8i0o9sSrtR3QYjTRWyl0qd5XDxWeutm_FmIj8v1-372FPcybxk2fWJHrpc1CU3j7u-BbIBogdQ2n2F12Z85AyVtPzVuYzNm-cnUVS3Cv1CKZFCi9C6GrDpr4Hz9JB_FbZG2XGL2NYjy8d2YcdhAuSe0ukA31m13dVoEHi3HySMvA_gj0gvlqr0teqHcBx3bn87y5a_Z49KYE-iQMc9Lh-UIK4YSZcMeLFm60YbmnK_T5A1EJ9aZB12w74xzFh9RyzXks7sk2EyMsGlvi97kXVH2CvwK-PD45TMGVD3-yG9o_GD4eRaH3w1rgU9XEDxrGm15VR1fsAvgGhkfyEt74nou7_vYvvO74Qo",
		"mimeType": "image/jpeg",
		"mediaMetadata": {
		  "creationTime": "2019-06-29T19:47:54Z",
		  "width": "4032",
		  "height": "2268",
		  "photo": {
			"cameraMake": "motorola",
			"cameraModel": "Moto G (5) Plus",
			"focalLength": 4.28
		  }
		},
		"filename": "IMG-20190629-WA0024-EFFECTS.jpg"
	  }
	],
	"nextPageToken": "CkgKQnR5cGUuZ29vZ2xlYXBpcy5jb20vZ29vZ2xlLnBob3Rvcy5saWJyYXJ5LnYxLkxpc3RNZWRpYUl0ZW1zUmVxdWVzdBICCAoSogFBSF91UTQzNFdSSmhFZHE0X2FNblFzZ1F5c3Riek1meUVWVmZUMWJ6VmNQelFPbDBjaEpUWVhSVVhXcm9yUG9rcVZ5TjJjRmdpZzlPUm5BMVgwbnlTUUFhS0Q3LTdQTDdNcXhmcGw1UXNwdktkUnVaeW5CeF94Nm9MdnpDdEd1cV80SkRoQUhiTkl1VENHeHUzMmdDN3dfVUdHZzJ2QUVEMVEaJ2dMU1piRnBNR3JSdXBsTGZmaFZIUWZBRnk2MDoxNmJhNWEzNDhkZA"
  }
  `

func TestUnmarshalListResponse(t *testing.T) {
	items := &MediaItems{}
	// target := &items

	body := bytes.NewBufferString(response)

	err := json.NewDecoder(body).Decode(items)
	if err != nil {
		t.Errorf("error decoding response: %s", err)
	}
	t.Logf("number of items returned: %d", len(items.Items))
}

func TestMediaItemsListCall(t *testing.T) {

}
