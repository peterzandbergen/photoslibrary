package photosapi

import (
	"context"
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"google.golang.org/api/option"
)

const (
	oauthCredentialsFile = "/home/peza/DevProjects/photoslibrary/secrets/oauth-creds.json"
	oauthTokenFile       = "/home/peza/DevProjects/photoslibrary/secrets/oauth-token.json"
)

func OpenReadAll(filename string) ([]byte, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("error opening %s: %s", filename, err)
	}
	defer f.Close()
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, fmt.Errorf("error reading bytes from %s: %s", oauthTokenFile, err)
	}
	return b, nil
}

func TestPhotos(t *testing.T) {
	b, err := OpenReadAll(oauthTokenFile)
	if err != nil {
		t.Fatalf("error reading bytes from %s: %s", oauthTokenFile, err)
	}
	ctx := context.Background()
	service, err := NewService(ctx, option.WithCredentialsJSON(b))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}
	_ = service
}

func TestMediaItemListPageSize(t *testing.T) {
	ctx := context.Background()
	service, err := NewService(ctx, option.WithCredentialsFile(oauthTokenFile))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}
	listcall := service.MediaItems.List()
	listcall.PageSize(2)
	items, err := listcall.Do()
	if err != nil {
		t.Errorf("error list do: %s", err)
	}
	if len(items.Items) != 2 {
		t.Errorf("expected %d items, received %d", 2, len(items.Items))
	}
}

func TestMediaItemListNextPage(t *testing.T) {
	ctx := context.Background()
	service, err := NewService(ctx, option.WithCredentialsFile(oauthTokenFile))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}

	listcall := service.MediaItems.List()
	listcall.PageSize(4)
	items_03, err := listcall.Do()
	if err != nil {
		t.Errorf("error list do: %s", err)
	}
	if len(items_03.Items) != 4 {
		t.Errorf("expected %d items, received %d", 4, len(items_03.Items))
	}

	listcall = service.MediaItems.List()
	listcall.PageSize(2)
	// listcall.PageToken()
	items_01, err := listcall.Do()
	if err != nil {
		t.Errorf("error list do: %s", err)
	}
	if len(items_01.Items) != 2 {
		t.Errorf("expected %d items, received %d", 2, len(items_01.Items))
	}

	listcall = service.MediaItems.List()
	listcall.PageSize(2)
	listcall.PageToken(items_01.NextPageToken)
	items_23, err := listcall.Do()
	if err != nil {
		t.Errorf("error list do: %s", err)
	}
	if len(items_23.Items) != 2 {
		t.Errorf("expected %d items, received %d", 2, len(items_23.Items))
	}
	if items_03.Items[3].Id != items_23.Items[1].Id {
		t.Errorf("id mismatch")
	}
}

func TestCountMediaItems(t *testing.T) {
	ctx := context.Background()
	service, err := NewService(ctx, option.WithCredentialsFile(oauthTokenFile))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}
	const pageSize = 100
	var nItems int64
	var nextPageToken string
	var found = make(map[string]struct{})
	for {
		// Get 100 items
		lc := service.MediaItems.List()
		lc.PageSize(pageSize)
		if len(nextPageToken) > 0 {
			lc.PageToken(nextPageToken)
		}
		resp, err := lc.Do()
		if err != nil {
			t.Errorf("error calling Do: %s", err)
			break
		}
		nextPageToken = resp.NextPageToken
		fmt.Fprintf(os.Stdout, "HTTPStatusCode: %d\n", resp.HTTPStatusCode)
		if len(resp.Items) == 0 {
			break
		}
		// add the ids to the map and break if the id already exists
		for _, i := range resp.Items {
			if _, ok := found[i.Id]; ok {
				fmt.Fprintf(os.Stdout, "found double: %s\n", i.FileName)
			}
			found[i.Id] = struct{}{}
		}
		nItems += int64(len(resp.Items))
		fmt.Fprintf(os.Stdout, "items: %d, nItems: %d\n", len(resp.Items), nItems)
		if len(nextPageToken) == 0 {
			break
		}
	}
	t.Logf("counted %d items", nItems)
}

func TestMediaItemsGet(t *testing.T) {
	const id = "AIG8HdbqF5lg3LXJNxy6T5ssdLSdWowF5a9BY3z1F6Tg1N7ShBzHMxMFY30OlEEI_o8xF9sKd2CGecngAeQ4DcEmuGru4fJslQ"
	const id1 = "AIG8HdaOtB8gB8l8iBoUq9bsx9JFwehL0k3CYzSxiG62dXBjpmEHoRVCD6rsMimQCWV_eUKZp9JgYKZryP2vhlm9bDnYcK9YnQ"

	ctx := context.Background()
	service, err := NewService(ctx, option.WithCredentialsFile(oauthTokenFile))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}
	mi, err := service.MediaItems.Get(id).Do()
	if err != nil {
		t.Errorf("error Get: %s", err)
	}
	t.Logf("%#v\n\n", mi)
	mi, err = service.MediaItems.Get(id1).Do()
	if err != nil {
		t.Errorf("error Get: %s", err)
	}
	t.Logf("%#v", mi)
}
