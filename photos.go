package photoslibrary

import (
	"context"
	"errors"

	"gitlab.com/peterzandbergen/photoslibrary/photosapi"

	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

var _ = iterator.Done

var (
	errNotImplementedYet = errors.New("not implemented yet")
)

// Client is the connection to the service.
type Client struct {
	svc *photosapi.Service
}

// NewClient returns a new client.
func NewClient(ctx context.Context, opts ...option.ClientOption) (*Client, error) {
	svc, err := photosapi.NewService(ctx, opts...)
	if err != nil {
		return nil, err
	}
	return &Client{
		svc: svc,
	}, nil
}

// MediaItem returns the media items with the given ID.
func (c *Client) MediaItem(id string) (*MediaItem, error) {
	mi, err := c.svc.MediaItems.Get(id).Do()
	if err != nil {
		return nil, err
	}
	return newMediaItem(mi), nil
}

// MediaItems returns all media items of the library as an iterator.
func (c *Client) MediaItems() *MediaItemIterator {
	return NewMediaItemIterator(c)
}

// Album returns the media items with the given ID.
func (c *Client) Album(id string) (*Album, error) {
	return nil, errNotImplementedYet
}

// Albums returns all media items of the library as an iterator.
func (c *Client) Albums() *AlbumIterator {
	return nil
}

// SharedAlbum returns the media items with the given ID.
func (c *Client) SharedAlbum(id string) (*SharedAlbum, error) {
	return nil, errNotImplementedYet
}

// SharedAlbums returns all media items of the library as an iterator.
func (c *Client) SharedAlbums() *SharedAlbumIterator {
	return nil
}
