package photoslibrary

import (
	"context"
	"testing"

	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

const (
	oauthCredentialsFile = "/home/peza/DevProjects/photoslibrary/secrets/oauth-creds.json"
	oauthTokenFile       = "/home/peza/DevProjects/photoslibrary/secrets/oauth-token.json"
)

func TestMediaItemIterator(t *testing.T) {
	ctx := context.Background()
	client, err := NewClient(ctx, option.WithCredentialsFile(oauthTokenFile))
	if err != nil {
		t.Errorf("error creating the service: %s", err)
	}
	it := client.MediaItems()
	_ = it

	for i := 0; i < 5; i++ {
		item, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			t.Errorf("error getting next: %s", err.Error())
		}
		t.Logf("%#v\n\n", item)
		// _ = item
	}
}
