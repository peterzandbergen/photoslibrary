package photoslibrary

import (
	"gitlab.com/peterzandbergen/photoslibrary/photosapi"

	"google.golang.org/api/iterator"
)

// MediaItem is a picture or a video.
type MediaItem struct {
	Id              string
	Description     string
	ProductUrl      string
	BaseUrl         string
	MimeType        string
	MediaMetadata   *MediaMetadata
	ContributorInfo *ContributorInfo
	FileName        string
}

type MediaMetadata struct {
	CreationTime string
	Width        string
	Height       string
	Photo        *Photo
	Video        *Video
}

type ContributorInfo struct {
	ProfilePictureBaseUrl string
	DisplayName           string
}

type Photo struct {
	CameraMake      string
	CameraModel     string
	FocalLength     float64
	ApertureFNumber float64
	IsoEquivalent   float64
	ExposureTime    string
}

type Video struct {
	CameraMake  string
	CameraModel string
	Fps         float64
	Status      string
}

// MediaItemIterator allows a caller to iterate over a set of MediaItems.
type MediaItemIterator struct {
	listcall *photosapi.MediaItemsListCall
	items    []*MediaItem
	pageInfo *iterator.PageInfo
	nextFunc func() error
}

// NewMediaItemIterator returns a new iterator.
func NewMediaItemIterator(c *Client) *MediaItemIterator {
	it := &MediaItemIterator{
		listcall: c.svc.MediaItems.List(),
	}
	it.pageInfo, it.nextFunc = iterator.NewPageInfo(
		it.fetch,
		func() int {
			return len(it.items)
		},
		func() interface{} {
			b := it.items
			it.items = nil
			return b
		},
	)
	it.pageInfo.MaxSize = 100
	return it
}

// PageInfo returns the internal page info struct.
func (it *MediaItemIterator) PageInfo() *iterator.PageInfo {
	return it.pageInfo
}

// fetch retrieves the next n items.
func (it *MediaItemIterator) fetch(pageSize int, pageToken string) (string, error) {
	it.listcall.PageToken(pageToken)
	it.listcall.PageSize(pageSize)
	mediaitems, err := it.listcall.Do()
	if err != nil {
		return "", err
	}
	// Copy the items.
	for _, item := range mediaitems.Items {
		it.items = append(it.items, newMediaItem(item))
	}
	return mediaitems.NextPageToken, nil
}

// Next returns the next item from the list.
// Returns iterator.Done when end of list has been reached.
func (it *MediaItemIterator) Next() (*MediaItem, error) {
	if err := it.nextFunc(); err != nil {
		return nil, err
	}
	item := it.items[0]
	it.items = it.items[1:]
	return item, nil
}

// newMediaItem creates a new media item from the photosapi mediaitem version.
// also creates the depending objects
func newMediaItem(i *photosapi.MediaItem) *MediaItem {
	return &MediaItem{
		Id:              i.Id,
		Description:     i.Description,
		ProductUrl:      i.ProductUrl,
		BaseUrl:         i.BaseUrl,
		MimeType:        i.MimeType,
		FileName:        i.FileName,
		MediaMetadata:   newMediaMetadata(i.MediaMetadata),
		ContributorInfo: newContributorInfo(i.ContributorInfo),
	}
}

func newMediaMetadata(m *photosapi.MediaMetadata) *MediaMetadata {
	if m == nil {
		return nil
	}
	return &MediaMetadata{
		CreationTime: m.CreationTime,
		Width:        m.Width,
		Height:       m.Height,
		Photo:        newPhoto(m.Photo),
		Video:        newVideo(m.Video),
	}
}

func newPhoto(p *photosapi.Photo) *Photo {
	if p == nil {
		return nil
	}
	return &Photo{
		CameraMake:      p.CameraMake,
		CameraModel:     p.CameraModel,
		FocalLength:     p.FocalLength,
		ApertureFNumber: p.ApertureFNumber,
		IsoEquivalent:   p.IsoEquivalent,
		ExposureTime:    p.ExposureTime,
	}
}

func newVideo(v *photosapi.Video) *Video {
	if v == nil {
		return nil
	}
	return &Video{
		CameraMake:  v.CameraMake,
		CameraModel: v.CameraModel,
		Fps:         v.Fps,
		Status:      v.Status,
	}
}

func newContributorInfo(c *photosapi.ContributorInfo) *ContributorInfo {
	if c == nil {
		return nil
	}
	return &ContributorInfo{
		ProfilePictureBaseUrl: c.ProfilePictureBaseUrl,
		DisplayName:           c.DisplayName,
	}
}
