module gitlab.com/peterzandbergen/photoslibrary

go 1.12

require (
	github.com/fatih/color v1.7.0
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/skratchdot/open-golang v0.0.0-20190402232053-79abb63cd66e
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	google.golang.org/api v0.7.0
)
