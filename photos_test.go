package photoslibrary

import (
	"context"
	"testing"
)

func TestCreateLibrary(t *testing.T) {
	ctx := context.Background()
	c, err := NewClient(ctx)
	if err != nil {
		t.Fatalf("error creating client: %s", err)
	}
	_ = c
}
