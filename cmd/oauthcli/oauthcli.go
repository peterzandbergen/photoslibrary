package main

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	pl "gitlab.com/peterzandbergen/photoslibrary/photosapi"

	"github.com/fatih/color"
	"github.com/skratchdot/open-golang/open"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

const (
	oauthCredentialsFile = "/home/peza/DevProjects/photoslibrary/secrets/oauth-creds.json"
	oauthTokenFile       = "/home/peza/DevProjects/photoslibrary/secrets/oauth-token.json"
)

var (
	done chan struct{}
	conf *oauth2.Config
	ctx  context.Context
)

func callbackHandler(w http.ResponseWriter, r *http.Request) {
	queryParts, _ := url.ParseQuery(r.URL.RawQuery)

	// Use the authorization code that is pushed to the redirect
	// URL.
	code := queryParts["code"][0]
	log.Printf("code: %s\n", code)

	// Exchange will do the handshake to retrieve the initial access token.
	tok, err := conf.Exchange(ctx, code)
	if err != nil {
		log.Fatal(err)
	}
	if err := SaveToken(oauthTokenFile, tok); err != nil {
		log.Printf("error saving token: %s", err)
	}

	log.Printf("Token: %s", tok)
	// The HTTP Client returned by conf.Client will refresh the token as necessary.
	client := conf.Client(ctx, tok)

	resp, err := client.Get("https://photos.google.com/")
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println(color.CyanString("Authentication successful"))
	}
	defer resp.Body.Close()

	// show succes page
	msg := "<p><strong>Success!</strong></p>"
	msg = msg + "<p>You are authenticated and can now return to the CLI.</p>"
	fmt.Fprintf(w, msg)
	// Signal we're done.
	close(done)
}

const (
	serviceAccountKey  = "service_account"
	userCredentialsKey = "authorized_user"
)

type OAuthClientCredentials struct {
	Type         string `json:"type"` // serviceAccountKey or userCredentialsKey
	ClientSecret string `json:"client_secret"`
	ClientID     string `json:"client_id"`
	RefreshToken string `json:"refresh_token"`
}

func SaveToken(filename string, tok *oauth2.Token) error {
	creds, err := LoadCredentials(oauthCredentialsFile)
	if err != nil {
		return err
	}
	creds.Type = userCredentialsKey
	creds.RefreshToken = tok.RefreshToken
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	err = json.NewEncoder(f).Encode(creds)
	return err
}

func LoadCredentials(filename string) (*OAuthClientCredentials, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	res := &OAuthClientCredentials{}
	err = json.NewDecoder(f).Decode(res)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func main() {
	creds, err := LoadCredentials(oauthCredentialsFile)
	if err != nil {
		log.Fatalf("error loading the oauth credentials: %s", err)
	}
	ctx = context.Background()
	conf = &oauth2.Config{
		ClientID:     creds.ClientID,
		ClientSecret: creds.ClientSecret,
		Scopes: []string{
			"openid",
			"profile",
			pl.PhotosLibraryReadOnlyScope,
		},
		Endpoint: google.Endpoint,
		// my own callback URL
		RedirectURL: "http://127.0.0.1:9999/oauth/callback",
	}

	// add transport for self-signed certificate to context
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	sslcli := &http.Client{Transport: tr}
	ctx = context.WithValue(ctx, oauth2.HTTPClient, sslcli)

	// Redirect user to consent page to ask for permission
	// for the scopes specified above.
	url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)

	log.Println(color.CyanString("You will now be taken to your browser for authentication"))
	time.Sleep(1 * time.Second)
	open.Run(url)
	time.Sleep(1 * time.Second)
	log.Printf("Authentication URL: %s\n", url)

	handler := http.NewServeMux()
	handler.HandleFunc("/oauth/callback", callbackHandler)

	server := &http.Server{}
	server.Handler = handler
	server.Addr = ":9999"
	done = make(chan struct{})
	go server.ListenAndServe()

	<-done
	log.Println("received done signal, shutting down the server")

	server.Shutdown(context.Background())
}
